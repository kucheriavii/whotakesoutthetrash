import React, { Component } from "react";
import Stage1 from "./components/stage1";
import Stage2 from "./components/stage2";
import { MyContext } from "./context";
import "./App.css"

class App extends Component {
  static contextType = MyContext;
  render(){
    console.log(this.context.state)
    return (
    <div className="wrapper">
      <div className="center-wrapper">
        <h1>Who will takes out the trash?</h1>
        {
          this.context.state.stage === 1 ?
            <Stage1 /> :
            <Stage2 />
        }
      </div>
    </div>
    )
  }
}

export default App;